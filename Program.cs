﻿namespace Loadr
{
	public class Program
	{
		public static void Main(string[] args)
		{
			new Processor(new PairProvider("DataFile.txt")).Load();
		}
	}
}